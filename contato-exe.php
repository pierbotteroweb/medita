<?php
// Carrega PHPMailer
require_once("PHPMailer/PHPMailerAutoload.php");

// Armazena os dados em variáveis escapando-os
$contato    = filter_input(INPUT_POST, 'contato', FILTER_SANITIZE_STRING);
$modal = filter_input(INPUT_POST, 'modal', FILTER_SANITIZE_STRING);


// Instanciando PHP Mailer
$mail = new PHPMailer;

if($contato == 'contato')
{
	$nome  		= filter_input(INPUT_POST, 'nomeTxt', FILTER_SANITIZE_STRING);
	$email 		= filter_input(INPUT_POST, 'emailTxt', FILTER_SANITIZE_EMAIL);
	$mensagem 	= filter_input(INPUT_POST, 'msgTxt', FILTER_SANITIZE_STRING);

	// Monta o html de envio de e-mail
	$strMensagem  = "<html>";
	$strMensagem .= "  <head><meta charset='utf-8'><title>Contato Medita</title></head>";
	$strMensagem .= "  <body>";
	$strMensagem .= "    <table>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td colspan='2'><h1 style='font-family: Arial;'>Mensagem de cliente pela landingpage Medita</h1></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Nome:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $nome . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>E-mail:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $email . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Mensagem:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $mensagem . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "    </table>";
	$strMensagem .= "  </body>";
	$strMensagem .= "</html>";
	$mail->Subject = 'Contato Site Medita - Mensagem de Visitante';
}

else if($modal == 'modal')
{
	$nome  		= filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
	$pessoa 		= filter_input(INPUT_POST, 'pessoaOpt', FILTER_SANITIZE_STRING);
	$numPessoaJuridica 		= filter_input(INPUT_POST, 'numPessoaJuridica', FILTER_SANITIZE_STRING);
	$numPessoaFisica 		= filter_input(INPUT_POST, 'numPessoaFisica', FILTER_SANITIZE_STRING);
	$estado 		= filter_input(INPUT_POST, 'estado', FILTER_SANITIZE_STRING);
	$tel1 		= filter_input(INPUT_POST, 'tel1', FILTER_SANITIZE_STRING);
	$tel2 		= filter_input(INPUT_POST, 'tel2', FILTER_SANITIZE_STRING);
	$email 		= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
	$site 		= filter_input(INPUT_POST, 'site', FILTER_SANITIZE_STRING);
	$atividades 		= filter_input(INPUT_POST, 'atividades', FILTER_SANITIZE_STRING);
	$mensagem 	= filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING);

	// Monta o html de envio de e-mail
	$strMensagem  = "<html>";
	$strMensagem .= "  <head><meta charset='utf-8'><title>Contato Faça Parte do Time Medita</title></head>";
	$strMensagem .= "  <body>";
	$strMensagem .= "    <table>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td colspan='2'><h1 style='font-family: Arial;'>Mensagem de cliente pela landing page Medita</h1></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Nome:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $nome . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Pessoa:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $pessoa . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>CPF:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $numPessoaFisica . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>CNPJ:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $numPessoaJuridica . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Estado:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $estado . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Telefone 1:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $tel1 . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Telefone 2:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $tel2 . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Endereço de email:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $email . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Site:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $site . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Atividades a oferecer no Medita:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $atividades . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>sobre o visitante:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $mensagem . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "    </table>";
	$strMensagem .= "  </body>";
	$strMensagem .= "</html>";

	$mail->Subject = 'Contato Site Medita - Mensagem de Parceiro';
}

$mail->CharSet = "UTF-8";
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = "smtp.medita.com.br";
$mail->Port = 587;
// $mail->SMTPSecure = 'tsl';
$mail->SMTPOptions = array(
	'ssl' => array(
		'verify_peer' => false,//
		'verify_peer_name' => false,
		'allow_self_signed' => true
	)
);

$mail->SMTPAuth = true;
$mail->Username = "naoresponda@medita.com.br";
$mail->Password = "parade04";
$mail->setFrom('naoresponda@medita.com.br', 'Contato do Site - Nome: '.$nome);
$mail->addReplyTo('contato@medita.com.br', 'Contato do Site: '.$nome);
$mail->addAddress('contato@medita.com.br', 'Contato Medita');
$mail->msgHTML($strMensagem);

if (!$mail->send()) {
	$retorno = array(
		'code' => 0,
	);

	echo json_encode($retorno);
} else {
	$retorno = array(
		'code' => 1,
	);

	echo json_encode($retorno);
}